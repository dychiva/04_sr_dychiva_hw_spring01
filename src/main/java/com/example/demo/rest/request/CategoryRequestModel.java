package com.example.demo.rest.request;

public class CategoryRequestModel {
    private int id;
    private String title;

    public CategoryRequestModel(){}

    public CategoryRequestModel(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
