package com.example.demo.rest.request;

public class BookRequestModel {

    private int id;
    private String title;
    private String author;
    private String description;
    private String thumbnail;
    private CategoryRequestModel category;

    public BookRequestModel() {}

    public BookRequestModel(int id, String title, String author, String description, String thumbnail, CategoryRequestModel category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public CategoryRequestModel getCategory() {
        return category;
    }

    public void setCategory(CategoryRequestModel category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "BookRequestModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", category=" + category +
                '}';
    }
}
