package com.example.demo.rest.restcontroller;


import com.example.demo.page.Pagination;
import com.example.demo.repository.dto.BookDto;
import com.example.demo.rest.request.BookRequestModel;
import com.example.demo.rest.response.BaseApiResponse;
import com.example.demo.service.impl.BookServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookRestController {
    private BookServiceImpl bookService;

    @Autowired
    public void setBookService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

//    insert Books
    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(
            @RequestBody BookRequestModel book){

        //System.out.println("Rest"+book);

        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();

        BookDto bookDto = mapper.map(book,BookDto.class);

        //System.out.println("Book DTO = " + bookDto);

        BookDto result = bookService.insert(bookDto);
        BookRequestModel result2 = mapper.map(result, BookRequestModel.class);
        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }

//    Select all books
    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> select(@RequestParam(value = "page" , required = false , defaultValue = "1") int page,
                                                                          @RequestParam(value = "limit" , required = false , defaultValue = "3") int limit,
                                                                          @RequestParam(value="categoryId",required = false,defaultValue = "0") int categoryId) {
        Pagination pagination = new Pagination(page, limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();


        pagination.setTotalCount(bookService.countBooks());
        pagination.setTotalPages(pagination.getTotalPages());

        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BookRequestModel>> response =
                new BaseApiResponse<>();

        List<BookDto> articleDtoList = bookService.select(pagination,categoryId);
        List<BookRequestModel> book = new ArrayList<>();

        for (BookDto articleDto : articleDtoList) {
            book.add(mapper.map(articleDto, BookRequestModel.class));
        }

        response.setPagination(pagination);
        response.setMessage("You have found all articles successfully");
        response.setData(book);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }


//get by ID
    @GetMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> getById(@PathVariable("id") int id){
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BookRequestModel>> response =
                new BaseApiResponse<>();

        BookDto articleDtoList = bookService.findById(id);
        List<BookRequestModel> book = new ArrayList<>();

        book.add(mapper.map(articleDtoList, BookRequestModel.class));
        response.setMessage("You have found all articles successfully");
        response.setData(book);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }


    //Delete Book
    @DeleteMapping("/books/{id}")
    public ResponseEntity<Void> delete (@PathVariable("id") int id){

        bookService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }



//Update Book
    @PutMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> update(
            @PathVariable("id") int id,
            @RequestBody BookRequestModel bookRequestModel){
        ModelMapper modelMapper=new ModelMapper();
        BookDto dto=modelMapper.map(bookRequestModel,BookDto.class);
        BookRequestModel responeModel=modelMapper.map(bookService.update(id,dto),BookRequestModel.class);

        BaseApiResponse<BookRequestModel> respone=new BaseApiResponse <>();
        respone.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        respone.setStatus(HttpStatus.OK);
        respone.setData(responeModel);
        respone.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(respone);
    }


}
