package com.example.demo.rest.restcontroller;

import com.example.demo.page.Pagination;
import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.dto.CategoryDto;
import com.example.demo.rest.request.CategoryRequestModel;
import com.example.demo.rest.request.CategoryRequestModel;
import com.example.demo.rest.response.BaseApiResponse;
import com.example.demo.service.impl.CategoryServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class CategoryRestController {
    private CategoryServiceImpl categoryService;

    @Autowired
    public void setCategoryService(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> select(@RequestParam(value = "page" , required = false , defaultValue = "1") int page,
                                                                              @RequestParam(value = "limit" , required = false , defaultValue = "2") int limit) {

        Pagination pagination = new Pagination(page, limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();


        pagination.setTotalCount(categoryService.countCategory());
        pagination.setTotalPages(pagination.getTotalPages());

        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<CategoryRequestModel>> response =
                new BaseApiResponse<>();

        List<CategoryDto> categoryDtoList = categoryService.select(pagination);
        List<CategoryRequestModel> book = new ArrayList<>();

        for (CategoryDto categoryDto : categoryDtoList) {
            book.add(mapper.map(categoryDto, CategoryRequestModel.class));
        }

        response.setPagination(pagination);
        response.setMessage("You have found all articles successfully");
        response.setData(book);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }


    //insert
    @PostMapping("/categories")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(
            @RequestBody CategoryRequestModel categoryRequestModel){

        //System.out.println("Rest"+book);

        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();

        CategoryDto categoryDto = mapper.map(categoryRequestModel,CategoryDto.class);

        //System.out.println("Book DTO = " + bookDto);

        CategoryDto result = categoryService.insert(categoryDto);
        CategoryRequestModel result2 = mapper.map(result, CategoryRequestModel.class);
        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    //select one

    @GetMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> getById(@PathVariable("id") int id){
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<CategoryRequestModel>> response =
                new BaseApiResponse<>();

        CategoryDto categoryDto = categoryService.findById(id);
        List<CategoryRequestModel> categoryRequestModels = new ArrayList<>();

        categoryRequestModels.add(mapper.map(categoryDto, CategoryRequestModel.class));
        response.setMessage("You have found all articles successfully");
        response.setData(categoryRequestModels);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

//    delete
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<Void> delete (@PathVariable("id") int id){
//        if(id == ){
//            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
//        }

        categoryService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


//    update
@PutMapping("/categories/{id}")
public ResponseEntity<BaseApiResponse<CategoryRequestModel>> update(
        @PathVariable("id") int id,
        @RequestBody CategoryRequestModel categoryRequestModel){
    ModelMapper modelMapper=new ModelMapper();
    CategoryDto dto=modelMapper.map(categoryRequestModel,CategoryDto.class);
    CategoryRequestModel responeModel=modelMapper.map(categoryService.update(id,dto),CategoryRequestModel.class);

    BaseApiResponse<CategoryRequestModel> respone = new BaseApiResponse <>();
    respone.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
    respone.setStatus(HttpStatus.OK);
    respone.setData(responeModel);
    respone.setTime(new Timestamp(System.currentTimeMillis()));
    return ResponseEntity.ok(respone);
}



}
