package com.example.demo.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@ComponentScan("com.example.demo")
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/v1/books").permitAll()
                .antMatchers(HttpMethod.GET,"/api/v1/categories").permitAll()
                .antMatchers(HttpMethod.POST,"/api/v1/books").hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/api/v1/categories").hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/v1/books/**").hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/v1/categories/**").hasAnyRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("user@gmail.com").password("{noop}user123").roles("USER");
        auth.inMemoryAuthentication().withUser("admin@gmail.com").password("{noop}admin123").roles("ADMIN");

    }
}
