package com.example.demo.uploadThumbnail;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
public class UploadThumbnailController {
    @PostMapping("/upload")
    public String upLoadFile(@RequestParam("file") MultipartFile file){
        File myUploadedFile = new File("\\D:\\KSHRD\\Spring\\Demo\\homework1\\src\\main\\java\\com\\example\\demo\\Image\\"+file.getOriginalFilename());

        try {
            myUploadedFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(myUploadedFile);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "localhost:8080/image/"+myUploadedFile.getName();
    }
}
