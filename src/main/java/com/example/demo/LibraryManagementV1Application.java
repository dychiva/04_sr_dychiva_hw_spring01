package com.example.demo;

import com.example.demo.configuration.SecurityConfiguration;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan("com.example.demo")
//@EnableWebMvc
@EnableSwagger2
@Import({SecurityConfiguration.class})
public class LibraryManagementV1Application {
	public static void main(String[] args) {

		SpringApplication.run(LibraryManagementV1Application.class, args);
	}

}
