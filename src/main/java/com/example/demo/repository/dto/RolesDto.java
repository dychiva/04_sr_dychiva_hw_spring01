package com.example.demo.repository.dto;

public class RolesDto {
    private int id;
    private String name;
    public RolesDto(){}

    public RolesDto(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RolesDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

