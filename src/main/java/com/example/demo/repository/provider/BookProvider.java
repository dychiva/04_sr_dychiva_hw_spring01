package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String select(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }


}
