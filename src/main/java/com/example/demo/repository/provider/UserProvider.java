package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    public String selectRolesByUserIdSql(String userId) {
        return new SQL(){{
            SELECT("r.id, r.name");
            FROM("tb_users r");
            INNER_JOIN("users_roles ur ON r.id = ur.role_id");
            WHERE("ur.user_id = '" + userId +"'");
        }}.toString();
    }

    public String selectUserByUsernameSql() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_users");
            WHERE("username = #{username}");
        }}.toString();
    }

    public String selectIdByUserId() {
        return new SQL(){{
            SELECT("id");
            FROM("tb_users");
            WHERE("user_id = #{userId}");
        }}.toString();
    }
}
