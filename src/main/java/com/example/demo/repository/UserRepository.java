package com.example.demo.repository;


import com.example.demo.repository.dto.RolesDto;
import com.example.demo.repository.dto.UserDto;
import com.example.demo.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {
//    @Select("SELECT * from tb_users WHERE email like #{email}")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "password", column = "password"),
//            @Result(property = "roles", column = "id", many = @Many(select ="FindRolesByUserId"))
//    })
//    UserDto loadUserByEmail(String email);
//
//    @Select("SELECT r.id, r.name FROM tb_roles r INNER JOIN tb_users_roles ur ON ur.role_id=r.id WHERE ur.user_id=#{id}")
//    List<UserDto> FindRolesByUserId(int id);

    @SelectProvider(type = UserProvider.class, method = "selectIdByUserId")
    int selectIdByUserId(String userId);

    @SelectProvider(type = UserProvider.class, method = "selectUserByUsernameSql")
    @Results({
            @Result(column = "user_id", property = "userId"),
            @Result(column = "id",
                    property = "roles",
                    many = @Many(select = "selectRolesByUserId"))
    })
    UserDto selectUserByUsername(String username);

    @Select("select r.id, r.name from tb_roles r\n" +
            "inner join users_roles ur on r.id = ur.role_id\n" +
            "where ur.user_id = #{id}")
    List<RolesDto> selectRolesByUserId(int id);
}
