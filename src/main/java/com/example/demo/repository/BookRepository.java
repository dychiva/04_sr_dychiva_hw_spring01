package com.example.demo.repository;


import com.example.demo.page.Pagination;
import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.dto.CategoryDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    //insert book
    @Insert("INSERT INTO tb_books ( title, author, description, thumbnail, category_id)" +
            "VALUES ( #{title}, #{author}, #{description}, #{thumbnail}, #{category.id})")
    boolean insert(BookDto book);


    //select book
    @Select("SELECT * FROM tb_books LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectAllCategory"))
    })
    List<BookDto> select(@Param("pagination") Pagination pagination);

    //select all category
    @Select("SELECT * FROM tb_categories WHERE id=#{category_id}")
    CategoryDto selectAllCategory(int category_id);


    //delete book
    @Delete("delete FROM tb_books where id =#{id}")
    void delete(int id);


    //findById in book
    @Select("SELECT * FROM tb_books WHERE id = #{id}")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectOneCategory"))
    })
    BookDto findById(int id);



    @Update("UPDATE tb_books set title = #{book.title}, author = #{book.author},thumbnail= #{book.thumbnail} ,description = #{book.description}, category_id= #{book.category.id} where id = #{id}")
    boolean update(int id, BookDto book);


    // count all book
    @Select("SELECT COUNT(id) FROM tb_books")
    int countBooks();


    //Searh book by category
    @Select("SELECT * FROM tb_books WHERE category_id=#{categoryId} LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    @Results({
            @Result(column = "category_id" ,property = "category",many = @Many(select = "selectOneCategory")),
    })
    List<BookDto> findCategoryId(@Param("pagination") Pagination pagination, @Param("categoryId") int categoryId);

    @Select("SELECT * FROM tb_categories WHERE id=#{category_id}")
    CategoryDto selectOneCategory(int category_id);

}
