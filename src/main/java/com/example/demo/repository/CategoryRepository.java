package com.example.demo.repository;


import com.example.demo.page.Pagination;
import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.dto.CategoryDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository  {

//    select
    @Select("SELECT * FROM tb_categories LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    List<CategoryDto> select(@Param("pagination") Pagination pagination);


//    insert
    @Insert("INSERT INTO tb_categories (title)"+
            "VALUES (#{title} )")
    boolean insert(CategoryDto categoryDto);


//    select one
    @Select("SELECT * FROM tb_categories WHERE id = #{id}")
    CategoryDto findById(int id);


//    delete
    @Delete("DELETE FROM tb_categories WHERE id =#{id}")
    void delete(int id);


//    update
    @Update("update tb_categories set title = #{categoryDto.title} WHERE id = #{id}")
    boolean update(int id,CategoryDto categoryDto);



//    count all category
    @Select("SELECT COUNT(id) FROM tb_categories")
    int countCategory();
}
