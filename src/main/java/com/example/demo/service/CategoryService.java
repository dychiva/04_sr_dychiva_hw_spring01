package com.example.demo.service;

import com.example.demo.page.Pagination;
import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.dto.CategoryDto;

import java.util.List;

public interface CategoryService {
    CategoryDto insert(CategoryDto categoryDto);
    List<CategoryDto> select(Pagination pagination);
    CategoryDto findById(int id);
    void delete(int id);
    CategoryDto update(int id, CategoryDto categoryDto);
    int countCategory();
}
