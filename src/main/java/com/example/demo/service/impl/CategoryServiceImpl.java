package com.example.demo.service.impl;

import com.example.demo.page.Pagination;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.dto.CategoryDto;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
   private CategoryRepository categoryRepository;

   @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDto insert(CategoryDto categoryDto) {
        boolean isInserted = categoryRepository.insert(categoryDto);
        if(isInserted)
            return categoryDto;
        else
            return null;
    }

    @Override
    public List<CategoryDto> select(Pagination pagination) {
        return categoryRepository.select(pagination);
    }

    @Override
    public CategoryDto findById(int id) {
       return categoryRepository.findById(id);
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }

    @Override
    public CategoryDto update(int id, CategoryDto categoryDto) {
        boolean isUpdated = categoryRepository.update(id,categoryDto);
        if(isUpdated){
            return categoryDto;
        }
        return null;
    }

    @Override
    public int countCategory() {
        return categoryRepository.countCategory();
    }
}
