package com.example.demo.service.impl;

import com.example.demo.page.Pagination;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.dto.BookDto;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.websocket.Session;
import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookRepository bookRepository;


    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookDto insert(BookDto book) {
        System.out.println("book"+book);
        boolean isInserted = bookRepository.insert(book);
        if(isInserted)
            return book;
        else
        return null;
    }



    @Override
    public List<BookDto> select(Pagination pagination, int categoryId) {
        if(categoryId == 0)
            return bookRepository.select(pagination);
        else
            return bookRepository.findCategoryId(pagination,categoryId);
    }

    @Override
    public BookDto findById(int id) {
        return  bookRepository.findById(id);
    }

    @Override
    public void delete(int id) {
//        BookDto bookDto = findById(id);
//        List<BookDto> bookDtos=  bookRepository.select();
//        bookDto.
        bookRepository.delete(id);
    }

    @Override
    public BookDto update(int id,BookDto bookDto) {
        boolean isUpdated = bookRepository.update(id,bookDto);
        if(isUpdated){
            return bookDto;
        }
        return null;
    }

    @Override
    public int countBooks() {
        return bookRepository.countBooks();
    }


}
