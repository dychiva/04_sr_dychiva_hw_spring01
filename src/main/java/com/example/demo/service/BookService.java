package com.example.demo.service;

import com.example.demo.page.Pagination;
import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.dto.CategoryDto;

import java.util.List;

public interface BookService {
    BookDto insert(BookDto book);
    List<BookDto> select(Pagination pagination, int categoryId);
    BookDto findById(int id);
    void delete(int id);
    BookDto update(int id, BookDto bookDto);
    int countBooks();

}
